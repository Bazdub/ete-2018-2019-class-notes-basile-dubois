\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{anysize}
\usepackage{enumerate}
\usepackage{amsmath, amsthm, amsfonts,amssymb}
\usepackage{graphicx}
\usepackage{cancel}
\marginsize{2cm}{2cm}{2cm}{2cm}
\renewcommand{\baselinestretch}{1.5}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{prop}{Proposition}[section]
\renewcommand\qedsymbol{$\blacksquare$}

\title{Econometrics II Notes}
\author{José Ignacio Hernández}
\date{\today}

\begin{document}

\begin{titlepage}
\begin{center}

	\vspace*{3.5cm}
	
	{\LARGE \textbf{Econometrics II Notes}}
	
	\vspace{2.5cm}
 
	{\Large \textbf{José Ignacio Hernández}\footnote{I made these notes during my studies in TSE}}
 
	Please let me know if there is something to add/remove/change to\\
	joseignaciohernandezh@gmail.com
 
	\vspace{1.5cm}
 
	\begin{large}
	Toulouse School of Economics\\
	Université Toulouse 1 - Capitole\\
	France\\
	\today
	\end{large}
 
\end{center}
\end{titlepage}

\section{Introduction to Time Series}

\begin{definition}
A \textbf{time series model} for the observed data $\{x_t\}$ is a specification of the joint distributions of a sequence of random variables $\{X_t\}$ of which $\{x_t\}$ is postulated to be a realization.
\end{definition}

TS allows us to understand phenomena given across time. In fact, TS are different on behavior than cross-sectional data. First of all, there is not such thing as a random sample, because TS are in nature not random. Notice also that there exists another phenomenon: volatility. 

So, first we need a set of assumptions and tools to estimate this TS models. 

\subsection{Stochastic Processes}

We need a mathematical model to explain data. We suppose that each point of data $x_t$ is a realization of a random variable $X_t$. Then, our time series $\{x_t, t \in T_0\}$ is a realization of a family of random variables $\{X_t, t\ in T_0\}$, and $X_t$ is also a realization of an \textbf{stochastic process} $\{X_t, t \in T\}$ such that $T \supseteq T_0$. 

\begin{definition}
A \textbf{stochastic process} is a family of Random Variables $\{ X_t, t \in \mathbf{T} \}$ defined in a probability space $(\Omega, \mathcal{F}, \mathcal{P})$.
\end{definition}

Notice that the interesting part of S.P. is that it depends of \textbf{time}. Time can be discrete or continuous.

\begin{definition}
The functions $\{ X(\omega), \omega \in \Omega \}$ are known as \textbf{realizations} or sample paths of the process $\{ X_t, t \in \mathbf{T} \}$
\end{definition}

Some examples of processes:

\begin{itemize}
	\item \textbf{Binary Process:} $X_t$ i.i.d. such that $P(X_t = 1) = P(X_t = -1) = 0.5$.
	\item \textbf{Random Walk:}
	\begin{gather*}
	X_t = \sum_{i=1}^{t} \varepsilon_i	\\
	X_t = \sum_{i=1}^{t-1} \varepsilon_i + \varepsilon_t	\\
	X_t = X_{t-1} + \varepsilon_t
	\end{gather*}
	\item \textbf{Trend Process:} $X_t = a + bt + \varepsilon_t$ with $\varepsilon_t$ i.i.d. and $E[\varepsilon_t] = 0$
\end{itemize}

\begin{definition}
%Distribution Function of S.P.
Let $\mathcal{I}$ be the set of all vectors $\{\mathbf{t} = (t_1,t_2,\ldots,t_n)' \in \mathbf{T}^n: t_1 < t_2 < \ldots < t_n, n=1,2,\ldots,n \}$. Then the \textbf{(finite-dimensional) distribution functions} of $\{X_t,t \in \mathbf{T} \}$ are the function $\{F_t(\cdot),\mathbf{t} \in \mathcal{I} \}$ defined for $\mathbf{t} = (t_1,t_2,\ldots,t_n)'$ by:

\begin{equation}
F_t(\mathbf{x}) = P(X_{t_1} < x_{1},X_{t_2} < x_{2},\ldots,X_{t_n} < x_{n}), \qquad \mathbf{x} = (t_1,t_2,\ldots,t_n) \in \mathbb{R}^n	\nonumber
\end{equation}
\end{definition}

The first part of the last definition is interesting. Suppose that we have $T = 0,1,2...$ Then the operator:

\begin{equation}
P(X_0 \leq x_0,...,X_t \leq x_0,...)	\nonumber
\end{equation}

is not mathematically precise. But:

\begin{gather}
\forall n, \forall t_1 < t_2,...<t_n	\nonumber	\\
P(X_{t_1} \leq x_1, X_{t_2}	\leq x_2,...) \nonumber
\end{gather}

It is well-defined. That gives sense for the latter definition.

The following theorem gives us a condition for the existence of well-defined probability distribution functions:

\begin{theorem}[Kolmogorov's Theorem]
The probability distribution functions $\{F_t(\cdot), \mathbf{t} \in \mathcal{I} \}$ are the distribution functions of some stochastic process if and only if for any $n \in \{1,2,\ldots\},\mathbf{t} = (t_1,t_2,\ldots,t_n)' \in \mathcal{I}$ and $1\leq i\leq n$,

\begin{equation}
\lim_{x_i \rightarrow \infty} F_\mathbf{t}(\mathbf{x}) = F_{\mathbf{t(i)}}(\mathbf{x(i)}) \nonumber
\end{equation}

with $\mathbf{t(i)} = (t_1,t_2,\ldots,t_{i-1},t_{i+1},\ldots,t_{n})'$ and $\mathbf{x(i)} = (x_1,x_2,\ldots,x_{i-1},x_{i+1},\ldots,x_{n})'$

\end{theorem}

Modelling TS with distribution functions is not common. We should use a Markov Process for convenience:

\begin{definition}
The process $\{ X_t \}$ is called a \textbf{Markov Process} if the conditional distribution of $X_{t+1}$ given $\{ X_\tau, \tau \leq t \}$ equals the distribution of $X_{t+1}$ given $X_t$, i.e:

\begin{equation}
X_{t+1} | \{X_\tau,\tau\leq t \} \stackrel{d}{=} X_{t+1} | X_t	\nonumber
\end{equation}
\end{definition}

For example:

\begin{align*}
P(X_1 \leq x_1, X_2 \leq x_2, \ldots, X_{t+1} \leq x_{t+1})
& = P(X_{t+1} \leq x_{t+1} | X_1 \leq x_1, X_2 \leq x_2, \ldots ) \\
& \times P(X_1 \leq x_1, X_2 \leq x_2, \ldots )
\end{align*}

In a Markov process that should be equal to:

\begin{equation}
P(X_{t+1} \leq x_{t+1} | X_t \leq x_t)	\nonumber
\end{equation}

It is useful to know the \textbf{Moment Generating Function}. Suppose $Y$ is a random variable. Then the Moment Generating Function is s.t:

\begin{align*}
M_Y(u) 	& =  E \left[ \exp \left(u Y \right) \right]	\\
		& =  E \left[ \sum_{j=0}^{\infty} \frac{u^j}{j!} Y^j \right]	\\
		& =  \sum_{j=0}^{\infty} \frac{u^j}{j!} E \left[ Y^j \right]	\\
\end{align*}

Evaluating in zero:

\begin{equation}
M_Y(0) 	= 1	\nonumber
\end{equation}

Now derivating AND then evaluating at zero:

\begin{equation}
M_Y^{(1)}(0) = \frac{E[Y^{(1)}]}{1!} = E[Y]	\nonumber
\end{equation}

For k-derivative\footnote{Check log-characteristic function}

\begin{equation}
M_Y^{(k)}(0) = E[Y^{(k)}]	\nonumber
\end{equation}

\subsection{Stationarity and Strict Stationarity}

With usual random variables, we can use the concept of \textit{covariance} to analyze the underlying relation between two random variables. In time series analysis, we can expand the definition to another concept to deal with infinite collections of random variables:

\begin{definition}
Consider a process $\{X_t,t \in \mathbf{T} \}$ such that $Var(X_t) < \infty$, for any $t \in \mathbf{T}$. Then the \textbf{autocovariance function} $\gamma(\cdot,\cdot)$ of $\{X_t\}$ is defined by:

\begin{equation}
\gamma(r,s) = Cov(X_r,X_s) = E[(X_r - E[X_r])(X_s - E[X_s])], \qquad r,s \in \mathbf{T}	\nonumber
\end{equation}
\end{definition}

Notice that our data is just one path of the Stochastic process, which for estimation processes is a problem: we have just one observation! Therefore, we need some additional regularity conditions in order to allow for estimation. The essential assumption is \textbf{stationarity}:

\begin{definition}
The Time Series $\{ X_t, t \in \mathbb{Z}\}$ such that  $\mathbb{Z} = \{ 0, +-1,+-2,...\}$ is said to be \textbf{second-order stationary} if:

\begin{itemize}
	\item $E[X_t^2] < \infty$, for all $t \in \mathbb{Z}$
	\item $E[X_t] = m$, for all $t \in \mathbb{Z}$
	\item $\gamma_X(r,s) = \gamma_X(r+t,s+t)$, for all $r,s,t \in \mathbb{Z}$.
\end{itemize}
\end{definition}

For example, a stationary process is such that:

\begin{equation}
E[X_1] = E[X_2] = ... = E[X_t]	\nonumber
\end{equation}

, for $r = s$:

\begin{gather*}
\gamma_X (r,r) = Var(X_r)	\\
\gamma_X (r+1,r+1) = Var(X_{r+1})	\\
\end{gather*}

So we have that $Var(X_1) = Var(X_2) = ...$. Then for $r \neq s$:

\begin{gather*}
Cov(X_r,X_s) = Cov(X_{r+t},X_{s+t})
\end{gather*}

Which implies that $Cov(X_1,X_2) = Cov(X_2,X_3) = Cov(X_3,X_4) = ...$

Careful with analyzing graphically stationarity, because we know nothing about conditional means.

If $\{X_t, t \in \mathbb{Z}\}$ is second-order stationary, then:

\begin{equation}
\gamma_X(r,s) = \gamma_X (r-s,0)\qquad ,\forall r,s \in \mathbb{Z}	\nonumber
\end{equation}

So, we redefine the autocovariance function for a stationary process as:

\begin{equation}
\gamma_X(h) = \gamma_X(h,0) = Cov(X_t,X_{t+h}), \qquad \forall t,h \in Z	\nonumber
\end{equation}

We can also define the \textbf{autocorrelation function} as:

\begin{equation}
\rho_X(h) = \frac{\gamma_X(h)}{\gamma_X(0)} = Corr(X_{t+h},X_t) \qquad \forall t,h \in \mathbb{Z}
\nonumber
\end{equation}

The \textbf{Partial Autocorrelation Function} is defined as follows: suppose a linear regression of $X_t$ over $X_{t-1}$, then to define PACF of order 1:

\begin{equation}
X_t = \alpha_0^{(1)} + \alpha_1^{(1)} X_{t-1} + \varepsilon_t^{(1)}	\nonumber
\end{equation}

With $\alpha_1$ as the PACF, s.t.:

\begin{equation}
\alpha_1 = \frac{Cov(X_t,X_{t-1})}{Var(X_{t-1}}	\nonumber
\end{equation}

Now for PACF of order 2:

\begin{equation}
X_t = \alpha_0^{(2)} + \alpha_1^{(2)} X_{t-1} + \alpha_2^{(2)} X_{t-2} + \varepsilon_t^{(2)}	\nonumber
\end{equation}

Then:

\begin{equation}
\begin{bmatrix}
\alpha_1^{(2)}	\\ \alpha_2^{(2)}
\end{bmatrix}
=
\begin{bmatrix}
Var(X_{t-1})	&	Cov(X_{t-1},X_{t-2})	\\
Cov(X_{t-1},X_{t-2})	&	Var(X_{t-2})
\end{bmatrix}^{-1}
\begin{bmatrix}
Cov(X_{t-1},X_{t})	\\ Cov(X_{t-2},X_{t})
\end{bmatrix}
\nonumber
\end{equation}

\begin{definition}
The time series $\{X_t, t \in \mathbb{Z}\}$ is said to be \textbf{strictly stationary} if the joint distributions of $(X_{t_1},X_{t_2},\ldots,X_{t_k})'$ and $(X_{t_1+h},X_{t_2+h},\ldots,X_{t_k+h})'$ are the same for all positive numbers $k$ and for all $t_1,t_2,\ldots,t_k,h \in \mathbb{Z}$.
\end{definition}

That means that $X_1 \stackrel{d}{=} X_2 \stackrel{d}{=} X_3 ...$, or in vectors:

\begin{gather*}
(X_1,X_2) \stackrel{d}{=} (X_2,X_3) \stackrel{d}{=} (X_3,X_4)...\\
(X_1,X_3) \stackrel{d}{=} (X_2,X_4) \stackrel{d}{=} ...
\end{gather*}

Strict stationary means on intuition that the graphs of 2 equal-length time series exhibits the same patterns graphically.

\begin{prop}
A strict stationary process with finite variance is a second-order stationary process.
\end{prop}

\begin{proof}
Suppose $\{X_t\}$ is strictly stationary. Then for $k=1$, $X_t$ has the same distribution for each $t \in \mathbb{Z}$. If $|X_t|^2 < \infty$, this implies that $E[X_t]$ and $V[X_t]$ are both constant. Moreover, if $k=2$, $X_{t+h}$ and $X_t$ have the same joint distribution, hence the same covariance for all $h \in \mathbb{Z}$. That proves the statement.

The converse is not true. Suppose $\{X_t\}$ is a sequence of independent random variables such that $X_t$ is exponentially distributed with mean one and variance one if $t$ is even, then $\{X_t\}$ is stationary with $\gamma_X(0) = 1$ and $\gamma_X(h) = 0$ for $h \neq 0$, but $X_1$ and $X_2$ have different distributions, so $\{X_t\}$ cannot be strictly stationary.
\end{proof}

\begin{definition}
The \textbf{ergodic (or stationary)} distribution of a strictly stationary process $\{X_t\}$ is the marginal distribution of $X_t$.
\end{definition}

\begin{prop}
The following processes are \textbf{non-stationary}:

\begin{itemize}
	\item A process with trend: $X_t = a + bt + \varepsilon_t$
	\item A process with \textbf{unit roots}: $X_t = X_{t-1} + \varepsilon_t$
	\item A process with \textbf{structural breaks}: $X_t = a_1 + \varepsilon_t$ if $t<T_1$ and $X_t = a_2 + \varepsilon_t$ if $t \geq T_1$.
\end{itemize}
\end{prop}

\begin{proof}
The proof for the first process is trivial, since the expectation depends of $t$. For the unit root process, taking expectation:

\begin{align*}
E[X_t] 	& = X_0 + \sum_{j=0}^{t-1} \varepsilon_{t-j}	\nonumber
		& = X_0
\end{align*}

Then for variance:

\begin{equation}
Var(X_t) = Var(X_{t-1}) + Var(\varepsilon_t) + 2 Cov(X_{t-1},\varepsilon_t)	\nonumber
\end{equation}
\end{proof}

\subsection{Linear Transformations of Processes}

\begin{definition}
The \textbf{lag operator} ($L$) transforms a process such that:

\begin{equation}
L^p X_t = X_{t-p}	\nonumber
\end{equation}
\end{definition}

Some properties of the lag operator:

\begin{itemize}
	\item $L X_t = X_{t-1}$
	\item $La = a$
	\item $L^p(a+b X_t) = a + b X_{t-p}$
	\item $L(X_t - Y_t) = X_{t-1} - Y_{t-1}$
\end{itemize}

\begin{definition}
The \textbf{difference operator} ($\Delta$) transforms a process such that:

\begin{equation}
\Delta X_t = X_t - X_{t-1}	\nonumber
\end{equation}
\end{definition}

For example:

\begin{align*}
\Delta^2 X_t 	& = \Delta (X_t - X_{t-1})	\\
				& = (X_t - X_{t-1}) - (X_{t-1} - X_{t-2})	\\
				& = X_t - 2X_{t-1} + X_{t-2}
\end{align*}

\begin{definition}
Given $a_0,a_1,\ldots,a_p \in \mathbb{R}$ with $p \in \mathbb{N}^+$ a \textbf{filter} is such that:

\begin{align*}
Y_t & \equiv a_0 X_t + a_1 X_{t-1} + a_2 X_{t-2} + \ldots + a_p X_{t-p}	\\
	& = (a_0 + a_1 L + a_2 L^2 + \ldots + a_p L^p) X_t	\\
	& = P(L) X_t
\end{align*}

With:

\begin{equation}
P(z) \equiv \sum_{i=0}^p a_i z^i	\nonumber
\end{equation}
\end{definition}
\section{ARMA Processes}

We present an important class of time series defined in terms of linear difference equations with constant coefficients.

\subsection{MA Processes}

\begin{definition}
Let $\{\varepsilon_t\}$ be a white noise and let $\psi_i, i=0,1,\ldots,q$ be a sequence of real numbers with $q< \infty$ and $\mu \in \mathbb{R}$. Then:

\begin{equation}
X_t = \mu + \sum_{i=0}^q \psi_i \varepsilon_{t-i}
\nonumber
\end{equation}

Is called a \textbf{Finite Moving Average} process of order $q$ or MA(q).
\end{definition}

Suppose we have a white noise $\varepsilon_t$, and we will take some numbers $\psi_0,\psi_1,\ldots,\psi_q$. So a finite MA process will take the form:

\begin{equation}
X_t = \mu + \varepsilon_t + \sum_{i=1}^q \psi_i \varepsilon_{t-i}	\nonumber
\end{equation}

\textbf{Question:} Is the MA process $\{x_t\}$ (second order) stationary? We should check:

\begin{itemize}
	\item Finite variance, $Var(X_t) < \infty$
	\item Constant mean, $E(X_t) = m$
	\item Constant covariance, $\gamma(r,s) = \gamma(r+t,s+t), \forall r,s,t \in \mathbb{Z}$
\end{itemize}

\begin{prop}
The finite MA(q) process is second-order stationary
\end{prop}

\begin{proof}
Taking expectations:

\begin{align*}
E(X_t) 	& = E(\mu) + E(\varepsilon_t) + \sum_{i=1}^q \psi_i E(\varepsilon_{t-i})	\\
		& = \mu
\end{align*}

Then for Variance:

\begin{align*}
Var(X_t) 	& = Var(\varepsilon_t + \sum_{i=1}^q \psi_i \varepsilon_{t-i} )	\\
			& = E\left[(\varepsilon_t + \sum_{i=1}^q \psi_i \varepsilon_{t-i})^2 \right]	\\
			& = \left(1 + \sum_{i=1}^q \psi_i^2 \right) \sigma^2_\varepsilon
\end{align*}

Since $E[\varepsilon_{t-s} \varepsilon_{t-h}] = 0$ for all $s \neq h$. Then for covariance, for $j = 1,2,\ldots,q$:

\begin{align*}
Cov(X_t,X_{t-j}) & =
E \left[ \left( \varepsilon_t + \sum_{k=1}^q \psi_k \varepsilon_{t-k} \right)
\left( \varepsilon_{t_j} + \sum_{k=1}^q \psi_k \varepsilon_{t-j-k} \right) \right]	\\
& = E \left( \psi_j \varepsilon^2_{t-j} + \psi_{j+1} \psi_1 \varepsilon^2_{t-j-1} + \ldots \right)	\\
& = E \left( \psi_j + \psi_{j+1} \psi_1 + \ldots \right) \sigma^2_\varepsilon
\end{align*}

For $j \leq q$. If $j > q$ there is no error term with common dates in the definition of covariance, so their expectation is zero. Thus, the MA(q) process is second-order stationary.
\end{proof}

\begin{definition}
Let $\{\varepsilon_t\}$ be a white noise and let $\psi_i$, with $i \in \mathbb{N}$, be a sequence pf reañ mi,bers sicj tjat $\sum_{i=0}^\infty \psi_i^2 < \infty$ and $\mu \in \mathbb{R}$. Then:

\begin{equation}
X_t = \mu + \sum_{i=0}^\infty \psi_i \varepsilon_{t-i}
\nonumber
\end{equation}

is called an \textbf{Infinite Moving Average} process, of $MA(\infty)$.
\end{definition}

\begin{prop}
The infinite MA process is stationary
\end{prop}

\begin{proof}
We have a white noise $\varepsilon_t$, and a sequence of numbers $\psi_i$. Then, the infinite order MA process is:

\begin{equation}
X_t = \mu + \sum_{i=0}^{\infty} \psi_i \varepsilon_{t-i}	\nonumber
\end{equation}

How can we handle this? Notice that we already know that the MA(q) process is stationary. So, taking the mean-square for $q < s$:

\begin{align*}
E \left[ (X_{t,q} - X_{t,s})^2 \right] & =
E \left[ (\sum_{i=0}^{q} \psi_i \varepsilon_{t-i} - \sum_{i=0}^{s} \psi_i \varepsilon_{t-i})^2 \right] 	\\
& = E \left[ \sum_{i=q+1}^s \psi_i \varepsilon_{t-i} \right]	\\
& = \left( \sum_{i=q+1}^s \psi_i^2 \right) \sigma^2_{\varepsilon}
\end{align*}

We assume that $\sum_{i=0}^\infty \psi^2_i < \infty$, then:

\begin{equation}
\forall \eta > 0, \exists i_0, \forall q,p > i_0: \left( \sum_{i=q+1}^s \psi_i^2 \right) < \eta	\nonumber
\end{equation}

So:

\begin{equation}
E \left[ (X_{t,q} - X_{t,s})^2 \right] < \eta \sigma_\varepsilon^2	\nonumber
\end{equation}

Then $X_{t,q} \rightarrow X_t$ as $q\rightarrow \infty$ and under $L^2$
\end{proof}

Therefore, the best linear projection of $\varepsilon_t$ on the Hilbert space generated by 1 and $X_{t-i},i=1,2,\ldots$ and denoted $H_{t-1}$ equals zero. Decompose the MA process in:

\begin{equation}
X_t = \mu + \varepsilon_t + \sum_{i=1}^q \psi_i \varepsilon_{t-i}	\nonumber
\end{equation}

Then:

\begin{align*}
Proj(X_t | H_{t-1}) 	& = \mu + \underbrace{Proj(\varepsilon_t|H_{t-1})}_{=0}	
						+ \sum_{i=1}^q \psi_i Proj(\varepsilon_{t-i}|H_{t-1})
\end{align*}

A sidenote:

\begin{equation}
Proj(A|B,C) = Proj(A|B) + Proj(A|C) - E(A)
\end{equation}

One should be tempted to say that $Proj(\varepsilon_{t-i}|H_{t-1}) = \varepsilon_{t-i}$, but careful. Let's take a MA(1) process with $\mu = 0$:

\begin{equation}
X_t = \varepsilon_t + \psi_1 \varepsilon_{t-i}	\\
\end{equation}

Then $H_t(X) \subset H_t(\varepsilon)$ but it is true that $H_t(\varepsilon_t) \subset H_t(X)$? Notice that:

\begin{align*}
\varepsilon_t 	& = X_t - \psi_1 \varepsilon_{t-1}	\\
				& = X_t - \psi_1 \left( X_{t-1} - \psi_1 \varepsilon_{t-2} \right)	\\
				& = X_t - \psi_1 X_{t-1} + \psi_1^2 \varepsilon_{t-2}	\\
				& = X_t - \psi_1 X_{t-1} + \psi_1^2 \left( X_{t-2} - \psi_1 \varepsilon_{t-3} \right)	\\
				& = X_t - \psi_1 X_{t-1} + \psi_1^2 X_{t-2} - \psi_1^3 \varepsilon_{t-3}	\\
				& = ...	\\
				& = \sum_{i=0}^h (-1)^i \psi^i X_{t-i}	+ (-1)^{h+1} \psi^{h+1} \varepsilon_{t-(h+1)}
\end{align*}

Which is not necessarily converging. The following theorem helps us in this case:

\begin{theorem}[Wold Decomposition]
Let $\{X_t\}$ be a second order stationary process. Then we have the representation:

\begin{equation}
X_t = E[X_t] + \sum_{i=0}^\infty \psi_i \varepsilon_{t-i} + \eta_t	\nonumber
\end{equation}

with:

\begin{enumerate}
	\item $\{\varepsilon_t\}$ is a second-order white noise process.
	\item $\psi_0 = 1$ and $\sum_{i=0}^\infty \psi_i^2 < \infty$
	\item $Proj(\varepsilon-t|H_t) = \varepsilon_t$
	\item $\{\eta_t\}$ is purely deterministic process
	\item $Proj(X_t|H_{t-1}) = E[X_t] + \sum_{i=1}^\infty \psi_i \varepsilon_{t-i} + \eta_t$
\end{enumerate}
\end{theorem}

This Theorem shows us that we can represent any second-order stationary process can be represented as an MA process plus a deterministic component.

\subsection{Spectral Density}

The spectral density of a stationary process with absolute summable autocovariance sequence is the Fourier transform of its ACF:

\begin{equation}
f_X(\omega) = \frac{1}{2 \pi} \sum_{i=-\infty}^\infty \gamma_X(h)e^{i\omega h}
\nonumber 
\end{equation}

One can recover the ACF function from the spectral density function:

\begin{equation}
\gamma_X(h) = \int_{-\pi}^\pi f_X(\omega) e^{-i\omega h} \partial \omega
\nonumber
\end{equation}

If:

\begin{equation}
\sum_{h=-\infty}^\infty | \gamma_X(h)| < \infty	\nonumber
\end{equation}

I can have $X_t$ second order stationary and:

\begin{equation}
\lim_{h\rightarrow \infty} \sum_{h=-\infty}^\infty | \gamma_X(h)| = \infty
\nonumber
\end{equation}

$X_t$ is a long memory process.

Take:

\begin{equation}
f_X (\omega) = \frac{\gamma_X(0)}{2 \pi} + \frac{2}{2 \pi} \sum_{i=1}^\infty \gamma_X (h) cos(\omega h)
\nonumber
\end{equation}

IDK why the above part.

Notice that if $f_X$ is constant:

\begin{align*}
\gamma_X (h) 	& = \int_{-\pi}^\pi f_X(\omega) e^{-i \omega h} \partial \omega	\\
				& = c \int_{-\pi}^\pi e^{-i \omega h} \partial \omega	\\
				& = 0
\end{align*}

Suppose we have the process:

\begin{equation}
Y_{t,q} = \sum_{j = -q}^{q} a_j X_{t-j}
\nonumber
\end{equation}

This process is stationary because the sum of stationary processes is also stationary\footnote{Proof!}. 

\textbf{Application:} Suppose that $X_t$ is an MA(1):

\begin{equation}
X_t = \varepsilon_t + \theta \varepsilon_{t-1} = (1+\theta L )\varepsilon_t
\nonumber
\end{equation}

I want to know if:

\begin{equation}
Proj(\varepsilon_t |1, X_t) = \varepsilon_t
\nonumber
\end{equation}

Under $|\theta| > 1$. Take the notation:

\begin{equation}
X_t = (1+\theta L) \varepsilon_t
\nonumber
\end{equation}

Taking variances:

\begin{align*}
Var(X_t) 	& = \sigma^2_\varepsilon + \theta^2 \sigma^2_\varepsilon	\\
			& = (1+ \theta^2) \varepsilon^2_\varepsilon
\end{align*}

Then the covariance:

\begin{align*}
\gamma_X(1) & = Cov(\varepsilon_t + \theta \varepsilon_{t-1},\varepsilon_{t-1} + \theta \varepsilon_{t-2} )	\\
			& = \theta \sigma^2_\varepsilon
\end{align*}

And $\gamma_X(h) = 0$ if $|h| > 1$. Then the correlation:

\begin{align*}
\rho_X(1) 	& = \frac{\theta \sigma^2_\varepsilon}{(1+\theta^2)\sigma^2_\varepsilon}	\\
			& = \frac{\theta}{1+\theta^2}	\\
			& = \frac{\cancel{\theta^2}}{\cancel{\theta^2}} \frac{\frac{1}{\theta}}
			{\left( \frac{1}{\theta} \right)^2 + 1}	\\
\end{align*}

The second equation is the ACF(1) of an MA(1) with $\theta$ as a moving average parameter, while the third one is the ACF(1) of an MA(1) with $\frac{1}{\theta}$ as a moving average paramenter.

Define $\eta_t$ as $\eta_t = (1+\theta^{-1} L)^{-1} (1+\theta L) \varepsilon_t$. Why? Because:

\begin{align*}
\eta_t 	& = (1+\theta^{-1} L)^{-1} (1+\theta L) \varepsilon_t	\\
		& = \left\{ \sum_{i=0}^\infty (-1)^i \theta^{-i} L^i \right\} (\varepsilon_t + \theta \varepsilon_{t-1})
\end{align*}

So:

\begin{equation}
X_t = (1+\theta^{-1} L) \eta_t	\nonumber
\end{equation}

Then, using the spectral density function:

\begin{align*}
f_\eta (\omega) & = f_\varepsilon (\omega) \frac{|1+\theta e^{i \omega}|^2}{1+\theta^{-1}e^{i\omega}|^2}	\\
				& = \frac{\sigma^2_\varepsilon}{2 \pi}
				\frac{(1+\theta \cos (\omega))^2 + \theta^2 \sin (\omega)^2}
				{(1+\theta^{-1} \cos (\omega))^2 + \theta^{-2} \sin (\omega)^2}	\\
				& = \frac{\sigma^2_\varepsilon}{2 \pi}
				\frac{1 + \theta^2 + 2\theta \cos (\omega)}
				{1 + \theta^{-2} + 2\theta^{-1} \cos (\omega)}	\\
				& = \frac{\sigma^2_\varepsilon}{2 \pi} \theta^2
				\frac{1 + \theta^{-2} + 2\theta^{-1} \cos (\omega)}
				{1 + \theta^{-2} + 2\theta^{-1} \cos (\omega)}	\\
				& = \frac{\sigma^2_\varepsilon \theta^2}{2 \pi}
\end{align*}

Therefore, $\eta_t$ is white noise with variance equal to $\sigma^2_\varepsilon \theta^2$.

Why we do this? We want to say that in:

\begin{equation}
Proj[X_t|1,X_{t-1}] = Proj[\varepsilon_t|1,X_{t-1}] + \theta Proj[\varepsilon_{t-1}|1,X_{t-1}]
\nonumber
\end{equation}

That $Proj[\varepsilon_{t-1}|1,X_{t-1}] = \varepsilon_{t-1}$. We know that this is true only if $|\theta| < 1$, in Wold decomposition. But we would like to transform the process to have that:

\begin{equation}
Proj[X_t|1,X_{t-1}] = \eta_t + \theta\eta_{t-1}	\nonumber
\end{equation}

And indeed. by transforming our process we have that $\varepsilon_t$ can be interpreted as the innovation of $X_t$.

In general, consider an MA(1) process such that:

\begin{equation}
X_t = P(L) \varepsilon_t
\end{equation}

Where $P(\zeta ) = \prod_{s=1}^S (\zeta - \zeta^*_{s,\mathbb{R}}) \prod_{j=1}^S (\zeta - \zeta_j^*)(\zeta - \zeta_j^*)$

...
Wold decomposition tells us that we can have a "nice" representation. When we go to the data, we should have 2 solution in an MA process. Then, which one is interesting in terms of terms of forecasting? In an MA(2) process for example, there is 4 possible solutions, but just one is interesting.

Finally, what we do is to convert $X_t = P(L) \varepsilon_t$ into $X_t = Q(L) \eta_t$, where $\eta_t = (Q(L))^{-1} P(L) \varepsilon_t$.

Consider an MA(2) process such that:

\begin{align*}
X_t	& = (1-\rho_1 L) (1-\rho_2 L) \varepsilon_t
	& = (1-(\rho_1 + \rho_2)L + \rho_1 \rho_2 L^2) \varepsilon_t
\end{align*}

With $\rho_1, \rho_2 \in \mathbb{R}$. Indeed, $X_t$ is stationary with $Cov(\varepsilon_t,X_t{t-i}) = 0, i>0$:

\begin{gather*}
E[X_t] = 0	\\
V[X_t] = \left(1+ (\rho_1 + \rho_2)^2 + (\rho_1 \rho_2)^2 \right) \sigma^2_\varepsilon
\end{gather*}

The covariance is:

\begin{align*}
Cov(X_t,X_{t-1})& = Cov(\varepsilon_t - (\rho_1 + \rho_2)\varepsilon_{t-1} + \rho_1 \rho_2 \varepsilon_{t-2},
						\varepsilon_{t-1} - (\rho_1 + \rho_2)\varepsilon_{t-2} + \rho_1 \rho_2 \varepsilon_{t-3})	\\
				& = \left( - (\rho_1 + \rho_2) - \rho_1 \rho_2(\rho_1 + \rho_2) \right) \sigma^2_\varepsilon
\end{align*}

\begin{equation}
Cov(X_t,X_{t-2}) = \rho_1 \rho_2 \sigma^2_\varepsilon \nonumber
\end{equation}

The projection is equal to:

\begin{align*}
Proj[X_{t+1}|1,X_t]& = Proj[\varepsilon_{t+1}-(\rho_1 + \rho_2)\varepsilon_t + \rho_1 \rho_2 \varepsilon_{t-1}|1,X_t]\\
				   & = 0 - \rho_1 \rho_2 Proj[\varepsilon_1|1,X_t] + \rho_1 \rho_2 Proj[\varepsilon_{t-1}|1,X_t]
\end{align*}

I don't know if this term if $\varepsilon_t$. This is true only if $|\rho_1|$ and $|\rho_2|$ are less than 1

We have 4 representations:

\begin{align*}
X_t & = (1-\rho_1 L)(1-\rho_2 L) \varepsilon_t	\\
	& = (1-\rho_1 L)(1-\rho_2^{-1} L) \eta_t	\\
	& = (1-\rho_1^{-1} L)(1-\rho_2^{-1} L) \psi_t	\\
	& = (1-\rho_1^{-1} L)(1-\rho_2 L) \nu_t
\end{align*}

With $\varepsilon_t,\eta_t,\psi_t,\nu_t$ white noise.

\textbf{Explanation:} Maybe your economic model tells you that $\rho_1 = 1$ But we don't know so we have to take some assumption for convenience, and theory gives you a hint. Data does not tell you which is the true specification, we have a problem of identification.

\subsection{AR Processes}

ARMA process are interesting, because they are linear, and thanks to Wold Decomposition we can transform any process in an MA process.

We know that if $X_t$ is an MA(q) process, $\gamma(h) = 0$ However, evidence suggest that we need a large $q$, which constraint our model in terms of flexibility.

We can truncate our process using truncation, but it imposes a constraint a priori. Another approach is to \textbf{parametrize our process} such that:

\begin{equation}
X_t = \mu_t + \sum_{i=0}^\infty \psi_t \mathbf{(\theta)} \varepsilon_t	\nonumber
\end{equation}

Notice the addition of $\theta$ in the model, such that $\theta \in \mathbb{R}^p$ with $p$ small, and $\sum \psi_t^2(\theta) < \infty$.

Maybe $\psi_i = \lambda/i$ or $\lambda \frac{m_i}{i}$ or whatever. We will specify $\psi_t = \rho^i$ such that $|\rho| <1$. Then:

\begin{align*}
X_t & = \mu + \sum_{i=0}^\infty \rho^i \varepsilon_{t-i}	\\
	& = \mu + \varepsilon_t + \rho \underbrace{\sum_{j=0,j=i-1}^\infty \rho^j \varepsilon_{t-1-j}}_{= \sum_{i=0}^\infty \rho^i \varepsilon_{t-i} \text{ lagged by 1}}	\\
	& = \mu + \varepsilon_t + \rho \left( X_{t-1} - \mu \right)	\\
	& = \mu(1-\rho) + \rho X_{t-1} + \varepsilon_t
\end{align*}

With $E[\varepsilon_t] = 0$, $Cov(\varepsilon_t,X_{t-1}) = 0$ and $Cov(\varepsilon_t,X_{t-i}) = 0$ for $i>0$. \textbf{This is an AR(1) process!} 

\begin{definition}
Let $\{\varepsilon_t\}$ be a white noise and $\phi_i, i=1,2,\ldots,p$ be a sequence of real numbers such that $p < \infty$ and $c \in \mathbb{R}$. Then:

\begin{equation}
X_t = c + \sum_{i=1}^p \phi_i X_{t-i} + \varepsilon_t	\nonumber
\end{equation}

Is an \textbf{Autoregressive Process} of order p, or AR(p).
\end{definition}

Suppose the simple case of an AR(1):

\begin{equation}
X_t = \mu + \rho X_{t-1} + \varepsilon_t
\nonumber
\end{equation}

It is $X_t$ stationary? Let see if $\rho = 1$:

\begin{gather*}
X_t = X_{t-1} + \varepsilon_t	\\
Var(X_t) = Var(X_{t-1}) + Var(\varepsilon_t)
\end{gather*}

So $X_t$ cannot be stationary. But if $|\rho|<1$:

\begin{equation}
X_t = \mu + \sum_{i=0}^\infty \rho^i \varepsilon_{t-i}
\end{equation}

Which fits with our initial definition of $\sum |\rho|^{2i} < \infty$ so $X_t$ is stationary.

Now if $|\rho|>1$:

\begin{align*}
X_t & = \rho X_{t-1} + \varepsilon_t	\\
\frac{1}{\rho} X_t & = X_{t-1} + \frac{1}{\rho} \varepsilon_t	\\
\frac{1}{\rho} X_{t-1} & = \frac{1}{\rho} X_{t} + \frac{1}{\rho} \varepsilon_t	\\
\frac{1}{\rho} X_{t-1} & = \frac{1}{\rho} \left( \frac{1}{\rho} X_{t+1} - \frac{1}{\rho} \varepsilon_{t+1}
						\right) - \frac{\varepsilon_t}{\rho}	\\
X_{t-1} & = \frac{1}{\rho^2} X_{t+1} - \frac{\varepsilon_t}{\rho} - \frac{\varepsilon_{t+1}}{\rho^2}	\\
\vdots	\\
X_{t-1} & = \frac{1}{\rho^{i+1}} X_{t+i} - \sum_{i=0}^\infty \frac{\varepsilon_{t+i}}{\rho^{i+1}}
\end{align*}

Notice that $\sum_{i=0}^\infty | \frac{1}{\rho^{i+1}}|^2 < \infty$ so we solved a part of our problem. What about the first sum?: If $|\rho| > 1$ I can find a stationary solution. It has the forward representation:

\begin{equation}
X_{t-1} = \frac{1}{\rho} X_t - \frac{1}{\rho} \varepsilon_t	\nonumber
\end{equation}

But it has a non-stationary solution! What is the following projection?:

\begin{align*}
Proj[X_{t+1}|1,X_t] & = Proj[\rho X_t + \varepsilon_{t+1}|1,X_t]	\\
					& = \rho X_t + Proj[\varepsilon_{t+1}|1,X_t]
\end{align*}

We don't know yet what is $Proj[\varepsilon_{t+1}|1,X_t]$. I start from $X_0$ suh that $\varepsilon_t \sim \text{iid}$ and:

\begin{equation}
X_t = \rho X_{t-1} + \varepsilon_t
\end{equation}

Then:

\begin{gather*}
Proj[X_1|1,X_0] = \rho X_0	\\
Proj[X_2|1,X_0] = Proj[\rho X_1|1,X_0] = \rho Proj[X_1|1,X_0] =\rho^2 X_0	\\
\vdots	\\
Proj[X_i|1,X_0] = \rho^i X_0
\end{gather*}

So, considering that $|\rho| > 1$ and making $i \rightarrow \infty$, $X_t$ is not stationary.

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\textwidth]{ar_explosive.pdf}
\caption{An explosive AR(1) process.}
\end{figure}
It seems a contradiction? From math perspective, it appears that $X_t$ is stationary if $|\rho| > 1$, however from a data perspective, our process is explosive, as shown in above figure.

Indeed:

\begin{align*}
X_t 	& = \rho X_{t-1} + \varepsilon_t	\\
X_{t-1}	& = \frac{1}{\rho} X_t + \frac{\varepsilon_t}{\rho}	\\
X_{t-1}	& = \frac{1}{\rho} F X_{t-1} + \tilde{\varepsilon}_t	\\
\left(1 - \frac{1}{\rho} F \right) X_{t-1}	& = \tilde{\varepsilon}_t	\\
X_{t-1}	& = \left(1 - \frac{1}{\rho} F \right)^{-1} \tilde{\varepsilon}_t	\\
X_{t-1}	& = \sum_{i=0}^{\infty} \left( \frac{1}{\rho} \right)^i F^i \tilde{\varepsilon}_t	\\
\end{align*}

With $F$ as the "Forward operator". So $X_t = \rho X_t + \varepsilon_t$ with $|\rho| > 1$.

Suppose that $\varepsilon \sim N(0,\sigma^2_\varepsilon)$, so $X_t \sim N(0,V)$ with:

\begin{equation}
V = \left(\sum_{i= 0}^\infty \left( \frac{1}{\rho} \right)^{2i} \frac{1}{\rho^2} \right) \sigma^2_\varepsilon
\nonumber
\end{equation}

\textbf{Recap 29/1/2019:} Suppose $X_t = \rho X_{t-1} + \varepsilon_t$ with $|\rho| >1$. Then there exists a stationary solution: suppose the forward solution:

\begin{align*}
X_{t-1} & = \frac{1}{\rho} X_t - \frac{1}{\rho} \varepsilon_t	\\
X_{t-1} - \frac{1}{\rho} X_t & =  - \frac{1}{\rho} \varepsilon_t	\\
\left(1 - \frac{1}{\rho} F \right) X_{t-1} & =  - \frac{1}{\rho} \varepsilon_t	\\
X_{t-1} & = \left(1 - \frac{1}{\rho} F \right)^{-1} \left(- \frac{1}{\rho} \varepsilon_t \right)	\\
		& = \left(- \frac{1}{\rho} \right) \left( \sum_{i=0}^{\infty} \rho^i F^i \right) \varepsilon_t	\\
		& = \left(- \frac{1}{\rho} \right) \sum_{i=0}^{\infty} \rho^i \varepsilon_{t+i}
\end{align*}

So take $X_0$ and $\{\varepsilon_t\}$ then:

\begin{gather*}
X_1 = \rho X_0 + \varepsilon_1	\\
X_2 = \rho X_1 + \varepsilon_2	\\
\vdots
\end{gather*}

So the projection $Proj(X_t|1,X_0) = \rho^t X_0$ and the process is stationary. But if $X_0 \neq 0$ the projection $Proj(X_t|1,X_0)$ tends to infinity so it is not stationary.

When $X_0$ we have the stationary solution as $X_0 = \left( - \frac{1}{\rho} \right) \sum_{i=0}^{\infty} \frac{1}{\rho} \varepsilon_{t+i}$.

Consider a deterministic sequence:

\begin{equation}
X_t = \rho X_{t-1}
\end{equation}

Think as a vector space. With $X,Y \in \mathcal{L}$ Then $\forall \alpha,\beta$:

\begin{equation}
\alpha X + \beta Y \in \mathcal{L}
\nonumber
\end{equation}

With $X_0$ given. For $t \geq 1$:

\begin{align*}
X_t & = \rho X_{t-1} = \rho^2 X_{t-2}	\\
\vdots	\\
	& = \rho^t X_0
\end{align*}

For $t<0$:

\begin{align*}
X_0 	& = \rho X_{-1}	\\
X_{1} 	& = \rho X_{0} + \varepsilon_{1}	\\
X_{2}	& = \rho X_1 + \varepsilon_2
\end{align*}

Let's back to AR processes.

Consider an AR(2) process:

\begin{align*}
(1-\phi_1 L - \phi_2 L^2)X_t = \varepsilon_t	\\
(1-\rho_1 L)(1-\rho_2 L)X_t = \varepsilon_t
\end{align*}

With $\rho_1$ and $\rho_2$ in $\mathbb{R}$. Notice that in an AR(1):

\begin{align*}
(1-\rho L) X_t & = \varepsilon_t	\\
X_t & = (1-\rho L)^{-1} \varepsilon_t
\end{align*}

If $|\rho|<1$ we indeed have the process $(\sum \rho^i L^i) \varepsilon_t$

In an AR(2):

\begin{align*}
(1-\rho_1 L)(1-\rho_2 L)X_t = \varepsilon_t	\\
(1-\rho_2 L)X_t = (1-\rho_1 L)^{-1}\varepsilon_t	\\
\end{align*}

Notice that $\rho_1$ and $\rho_2$ are the inverse of the roots of the polynomial:

\begin{equation}
P_2(z) = 1 - \phi_1 z - \phi_2 z^2
\nonumber
\end{equation}

Notice that $\phi_1^2 + 4 \phi_2 <0$, so we have a complex solution:

\begin{gather*}
\rho_1^{-1} = z_1^* = r e ^{i\theta} = r (\cos\theta + i \sin \theta)	\\
\rho_2^{-1} = z_2^* = r e ^{i\theta} = r (\cos\theta + i \sin \theta)	\\
|z_1^*| + |z_2^*| = r
\end{gather*}

With $r > 1$

Then:

\begin{align*}
(1-\phi_1 z - \phi_2 z^2) 	& = (1-\frac{z}{z_1^*}) (1-\frac{z}{z_2^*})	\\
							& = (1-\rho_1 z) (1-\rho_2 z)	\\
(1-\rho_1 L)^{-1}			& = \sum_{i=0}^\infty \rho_1^i L^i \\
(1-\rho_2 L)^{-1}			& = \sum_{i=0}^\infty \rho_2^i L^i
\end{align*}

And:

\begin{equation}
\frac{1}{1-\rho z} = \sum_{i=0}^\infty \rho^i z^i
\nonumber
\end{equation}

\begin{align*}
X_t & = (1- \rho_1 L )^{-1} (1- \rho_2 L )^{-1} \varepsilon_t	\\
	& = \left\{ \sum_{i=0}^{\infty} \rho_1^i L^i \right\} \left\{ \sum_{j=0}^{\infty} \rho_2^j L^j \right\}	\varepsilon_t\\
	& = \left( \sum_{i=0}^{\infty} \sum_{j=0}^{\infty} \rho_1^i \rho_2^j L^{i+j} \right) \varepsilon_t	\\
	& = \left( \sum_{k=0}^{\infty} a_k L^k \right) \varepsilon_t
\end{align*}

Where:

\begin{align*}
a_k & = \sum_{j=0}^k \rho_1^j \rho_2^{k-j}	\\
	& = \sum_{i=0}^j \frac{1}{rj} e^{\iota \theta j} \frac{e^{i\theta(k-j)}}{r^{k-j}}	\\
	& = \frac{1}{r^k} \sum_{j=0}^k e^{i \theta(k - 2j)}
\end{align*}

If $k=0$, $e^{i\theta(0-2*0)} = e^0 = 1$ and i $k = 1$:

\begin{align*}
e^{i\theta(1-0)} + e^{i\theta(1-2)}	\\
= e^{i\theta} + e^{-i\theta}	\\
= 2 \cos\theta
\end{align*}

So:

\begin{align*}
a_k 	& = \frac{1}{r^j} \sum_{j=0}^k \cos(\theta(k-2j))	\\
|a_k|	& = \frac{1}{r^j} \left| \sum_{j=0}^k \cos(\theta(k-2j)) \right|	\\
		& \leq \frac{\left| \sum_{j=0}^k \cos(\theta(k-2j)) \right|}{r^j} \\
		& \leq \frac{k+1}{r^k}
\end{align*}

Define $b_k = \frac{k+1}{r^k}$. How can I make that $\sum b_k < \infty$? Well, notice that:

\begin{align*}
\frac{b_{k+1}}{b_{k}} 	& = \frac{b_{k+2}}{b_{k+1}} \frac{r^k}{r^{k+1}}	\\
						& = \frac{b_{k+2}}{b_{k+1}} \frac{1}{r}
\end{align*}

Now $\forall \eta > 0, \exists k_0, \forall k \geq k_0$:

\begin{align*}
\left| \frac{k_{t+1}}{k_t} \right| 	& < (1+\eta)\frac{1}{r}	\\
							b_{k+1} & < b_k \frac{1+\eta}{r}	\\
									& < b_{k-1} \left( \frac{1+\eta}{r}	\right)^2	\\
									& < b_{k_0} \left( \frac{1+\eta}{r} \right)^{k-k_0+1}
\end{align*}

So:

\begin{align*}
\sum_{k=k_0}^K b_k 	& \leq b_{k_0} \sum_{k=k_0}^K \left( \frac{1+\eta}{r} \right)^{k-k_0+1}	\\
					& 
\end{align*}

\end{document}